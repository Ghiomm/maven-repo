<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>


	<groupId>net.pickcellslab.foundationj</groupId>
	<artifactId>pom-foundationj</artifactId>
	<version>0.4.0</version>
	<packaging>pom</packaging>

	<name>foundationJ Projects</name>
	<url>http://Ghiomm.bitbucket.org/</url>
	<inceptionYear>2014</inceptionYear>

	<organization>
		<name>foundationj</name>
		<url>http://Ghiomm.bitbucket.org/</url>
	</organization>

	<developers>
		<developer>
			<id>gblin</id>
			<name>Guillaume Blin</name>
			<email>guillaume.blin@ed.ac.uk</email>
			<organization>MRC Center for Regenerative Medicine</organization>
			<organizationUrl>http://www.crm.ed.ac.uk/</organizationUrl>
			<roles>
				<role>maintainer</role>
				<role>developer</role>
				<role>architect</role>
			</roles>
			<timezone>+0</timezone>
		</developer>
	</developers>




	<properties>
		<!-- Manage dependencies for Logging -->
		<slf4j.version>1.7.7</slf4j.version>
		<swingx.version>1.6.4</swingx.version>
		<commons-math.version>3.4.1</commons-math.version>
		<javaluator.version>3.0.0</javaluator.version>
	</properties>






	<dependencyManagement>
		<dependencies>

			<!-- Modules versions management -->
			<dependency>
				<groupId>net.pickcellslab.foundationj</groupId>
				<artifactId>foundationj-datamodel</artifactId>
				<version>${project.version}</version>
			</dependency>
			<dependency>
				<groupId>net.pickcellslab.foundationj</groupId>
				<artifactId>foundationj-dbm</artifactId>
				<version>${project.version}</version>
			</dependency>
			<dependency>
				<groupId>net.pickcellslab.foundationj</groupId>
				<artifactId>foundationj-queryui</artifactId>
				<version>${project.version}</version>
			</dependency>
			<dependency>
				<groupId>net.pickcellslab.foundationj</groupId>
				<artifactId>foundationj-views</artifactId>
				<version>${project.version}</version>
			</dependency>
			<dependency>
				<groupId>net.pickcellslab.foundationj</groupId>
				<artifactId>foundationj-optim</artifactId>
				<version>${project.version}</version>
			</dependency>
			<dependency>
				<groupId>net.pickcellslab.foundationj</groupId>
				<artifactId>foundationj-appli</artifactId>
				<version>${project.version}</version>
			</dependency>
			<dependency>
				<groupId>net.pickcellslab.foundationj</groupId>
				<artifactId>foundationj-boot</artifactId>
				<version>${project.version}</version>
			</dependency>

			<!-- Logging -->
			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>slf4j-api</artifactId>
				<version>${slf4j.version}</version>
				<scope>compile</scope>
			</dependency>
			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>jcl-over-slf4j</artifactId>
				<version>${slf4j.version}</version>
				<scope>compile</scope>
			</dependency>
			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>slf4j-log4j12</artifactId>
				<version>${slf4j.version}</version>
				<scope>compile</scope>
			</dependency>

			<!-- swingx -->
			<dependency>
				<groupId>org.swinglabs.swingx</groupId>
				<artifactId>swingx-all</artifactId>
				<version>${swingx.version}</version>
			</dependency>

			<!-- Maths commons -->
			<dependency>
				<groupId>org.apache.commons</groupId>
				<artifactId>commons-math3</artifactId>
				<version>${commons-math.version}</version>
				<scope>compile</scope>
			</dependency>

			<!-- Javaluators -->
			<dependency>
				<groupId>com.fathzer</groupId>
				<artifactId>javaluator</artifactId>
				<version>${javaluator.version}</version>
				<scope>compile</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>




	<!-- Manage build plugins -->

	<build>

		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.codehaus.mojo</groupId>
					<artifactId>license-maven-plugin</artifactId>
					<configuration>
						<licenseName>gpl_v3</licenseName>
						<organizationName>Guillaume Blin</organizationName>
						<projectName>foundationJ : a lightweight framework that provides a
							programing model to build desktop applications with a
							self-descriptive graph data management system</projectName>
					</configuration>
				</plugin>


				<plugin>
					<groupId>org.codehaus.mojo</groupId>
					<artifactId>versions-maven-plugin</artifactId>
					<version>2.1</version>
					<configuration>
						<includes>
							<include>net.pickcellslab.foundationj:*</include>
						</includes>
					</configuration>
				</plugin>


				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>3.2</version>
					<configuration>
						<source>1.8</source>
						<target>1.8</target>
					</configuration>
				</plugin>

			</plugins>

		</pluginManagement>


		<extensions>
			<extension>
				<groupId>ar.com.synergian</groupId>
				<artifactId>wagon-git</artifactId>
				<version>0.3.0</version>
			</extension>
		</extensions>


	</build>



	<pluginRepositories>
		<pluginRepository>
			<id>synergian-repo</id>
			<url>https://raw.github.com/synergian/wagon-git/releases</url>
		</pluginRepository>
	</pluginRepositories>



	<distributionManagement>
		<repository>
			<id>fjmvn</id>
			<name>fjmvn</name>
			<url>git:releases://git@homeid:Ghiomm/maven-repo.git</url>
		</repository>
	</distributionManagement>



	<repositories>
		<!-- foundationj repository in bitbucket -->
			<repository>
				<id>fjmvn</id>
				<name>fjmvn</name>
				<releases>
					<enabled>true</enabled>
				</releases>
				<snapshots>
					<enabled>false</enabled>
				</snapshots>

				<url>https://api.bitbucket.org/1.0/repositories/Ghiomm/maven-repo/raw/releases</url>
			</repository>
	</repositories>


</project>